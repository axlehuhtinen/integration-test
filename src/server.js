const { request } = require('express');
const express = require('express');
const converter = require('./converter');
const app = express();
const PORT = 3000;

// Welcome endpoint
app.get('/', (req, res) => res.send("Welcome!"));

// RGB to Hex endpoint
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.r, 10);
    const green = parseInt(req.query.g, 10);
    const blue = parseInt(req.query.b);
    res.send(converter.rgbToHex(red,green,blue));
});

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server listening..."));
}
